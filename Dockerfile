# to run: `docker run souljabot`
FROM python:3.5-onbuild

ENTRYPOINT ["python", "main.py"]