# SouljaBot

initial telegram bot


## docker commands

for simplicity i made build_n_push.sh for image


1. to build:
    - docker build -t souljabot .
2. change tag so it can be pushed to registry:
    - docker tag souljabot registry.ng.bluemix.net/soulja/souljabot:latest
3. to push to ibm:
    - docker push registry.ng.bluemix.net/soulja/souljabot:latest
4. to run on ibm bluemix:
    - cf ic run --name soulja registry.ng.bluemix.net/soulja/souljabot:latest

## todo:
