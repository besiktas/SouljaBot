#!/bin/sh
# simple script to build, change tag name push to bluemix registry and then run

docker build -t souljabot .
docker tag souljabot registry.ng.bluemix.net/soulja/souljabot:latest
docker push registry.ng.bluemix.net/soulja/souljabot:latest
cf ic run --name soulja registry.ng.bluemix.net/soulja/souljabot:latest